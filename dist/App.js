import React from 'react';
import './App.css';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { CssBaseline, Container } from '@material-ui/core';
import TableOfUsersWithSampleData from './components/TableOfUsersWithSampleData';
const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        minHeight: '100vh'
    },
    paper: {
        padding: theme.spacing(3, 2),
    },
    container: {
        padding: theme.spacing(3, 0),
    }
}));
const App = () => {
    const classes = useStyles();
    return (React.createElement("div", { className: classes.root },
        React.createElement(CssBaseline, null),
        React.createElement(AppBar, { position: "static", color: "primary" },
            React.createElement(Toolbar, null,
                React.createElement(Typography, { variant: "h6", color: "inherit" }, "Users"))),
        React.createElement(Container, { fixed: true, className: classes.container },
            React.createElement(TableOfUsersWithSampleData, { withSearch: true }))));
};
export default App;
//# sourceMappingURL=App.js.map