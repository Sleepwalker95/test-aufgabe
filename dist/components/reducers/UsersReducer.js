const initialState = {};
const ACTION_SORT_BY = "actions-users-sort-by";
const ACTION_FILTER = "actions-users-filter";
export function sortUsersBy(tableId, sortRowId, descending) {
    return {
        type: ACTION_SORT_BY,
        tableId,
        sortRowId, descending
    };
}
export function filterUsers(tableId, filter) {
    return {
        type: ACTION_FILTER,
        tableId,
        filter
    };
}
export default function UsersReducer(state = initialState, action) {
    switch (action.type) {
        case ACTION_SORT_BY: {
            const sortAction = action;
            var sortPayload = Object.assign({}, state);
            sortPayload[action.tableId] = Object.assign({}, sortPayload[action.tableId], { sortRowId: sortAction.sortRowId, descending: sortAction.descending });
            return sortPayload;
        }
        case ACTION_FILTER: {
            const filterAction = action;
            var filterPayload = Object.assign({}, state);
            filterPayload[action.tableId] = Object.assign({}, filterPayload[action.tableId], { filter: filterAction.filter });
            return filterPayload;
        }
    }
    return state;
}
//# sourceMappingURL=UsersReducer.js.map