import React from 'react';
import { RowItem as XRowItem } from './XTableHead';
export interface RowItem extends XRowItem {
    key: string;
    transformData?: (data: any) => React.ReactNode;
}
interface Props {
    data: any[];
    rows: RowItem[];
    tableId: string;
    withSearch?: boolean;
}
declare const _default: (props: Props) => JSX.Element;
export default _default;
