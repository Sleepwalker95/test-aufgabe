declare const rootReducer: import("redux").Reducer<{
    usersConfig: import("./UsersReducer").UsersState;
}, import("redux").AnyAction>;
export default rootReducer;
export declare type ReduxAppState = ReturnType<typeof rootReducer>;
