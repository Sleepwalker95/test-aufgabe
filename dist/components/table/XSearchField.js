import React from 'react';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import { fade, makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles(theme => ({
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
}));
const XSearchField = (props) => {
    const { value, onChange } = props;
    const classes = useStyles({});
    return (React.createElement("div", { className: classes.search },
        React.createElement("div", { className: classes.searchIcon },
            React.createElement(SearchIcon, null)),
        React.createElement(InputBase, { placeholder: "Search\u2026", value: value, onChange: (event) => {
                onChange && onChange(event.target.value);
            }, classes: {
                root: classes.inputRoot,
                input: classes.inputInput,
            }, inputProps: { 'aria-label': 'Search' } })));
};
export default XSearchField;
//# sourceMappingURL=XSearchField.js.map