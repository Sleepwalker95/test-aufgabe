import React from 'react';
declare const CopyIdButton: React.SFC<{
    copyId: string;
    buttonTitle?: string;
}>;
export default CopyIdButton;
