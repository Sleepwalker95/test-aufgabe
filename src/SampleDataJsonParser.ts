var data = require('./assets/sample_data.json');

/**
 * Simple parser for randomuser.me data
 */
export function parseSampleData() {
    console.log(data)

	const jsonStripped = data.results.map(user => {
		return {
			id: user.login.uuid,
			firstname: user.name.first,
			lastname: user.name.last,
			username: user.login.username, 
			email: user.email,
			birthdate: user.dob.date,
			picture: user.picture
		}
	})

	console.log(JSON.stringify(jsonStripped))
}