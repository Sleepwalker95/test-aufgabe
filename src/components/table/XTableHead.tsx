import React from 'react'
import { TableHead, TableRow, TableCell, TableSortLabel } from '@material-ui/core';

export interface RowItem {
    id: number,
    numeric: boolean,
    label: string,
    canSortBy?: boolean
}

const XTableHead: React.SFC<{orderDescending?: boolean, orderBy?: number, headRows: RowItem[], onSortByClick?: (row: RowItem, orderDescending: boolean)=>void}> = (props) => {
    const { orderDescending, orderBy, headRows, onSortByClick } = props
    const onRowSortClick = (row: RowItem) => { 
        const desc = row.id===orderBy && !orderDescending ? true : false
        onSortByClick && onSortByClick(row, desc)
    }
  
    return (
        <TableHead>
            <TableRow>
            {headRows.map(row => (
                <TableCell
                    key={row.id}
                    align={row.numeric ? 'right' : 'left'}
                    padding={'default'}
                    sortDirection={orderBy === row.id ? (Boolean(orderDescending) ? "desc" : "asc") : false}>
                    {
                        Boolean(row.canSortBy) ?
                        <TableSortLabel
                            active={orderBy === row.id}
                            direction={Boolean(orderDescending) ? "desc" : "asc"}
                            onClick={()=>onRowSortClick(row)}>
                            {row.label}
                        </TableSortLabel>
                        :
                        row.label
                    }
                </TableCell>
            ))}
            </TableRow>
        </TableHead>
    );
}

export default XTableHead