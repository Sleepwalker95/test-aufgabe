import React from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from '../reducers';
const store = createStore(rootReducer);
const ReduxProvider = (props) => {
    return (React.createElement(Provider, { store: store }, props.children));
};
export default ReduxProvider;
//# sourceMappingURL=ReduxProvider.js.map