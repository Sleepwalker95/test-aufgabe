import React from 'react';
import { TableHead, TableRow, TableCell, TableSortLabel } from '@material-ui/core';
const XTableHead = (props) => {
    const { orderDescending, orderBy, headRows, onSortByClick } = props;
    const onRowSortClick = (row) => {
        const desc = row.id === orderBy && !orderDescending ? true : false;
        onSortByClick && onSortByClick(row, desc);
    };
    return (React.createElement(TableHead, null,
        React.createElement(TableRow, null, headRows.map(row => (React.createElement(TableCell, { key: row.id, align: row.numeric ? 'right' : 'left', padding: 'default', sortDirection: orderBy === row.id ? (Boolean(orderDescending) ? "desc" : "asc") : false }, Boolean(row.canSortBy) ?
            React.createElement(TableSortLabel, { active: orderBy === row.id, direction: Boolean(orderDescending) ? "desc" : "asc", onClick: () => onRowSortClick(row) }, row.label)
            :
                row.label))))));
};
export default XTableHead;
//# sourceMappingURL=XTableHead.js.map