import React from 'react';
declare const TableOfUsersWithSampleData: React.SFC<{
    tableId?: string;
    withSearch?: boolean;
}>;
export default TableOfUsersWithSampleData;
