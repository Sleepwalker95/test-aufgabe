import React from 'react'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from '../reducers'

const store = createStore(rootReducer)

const ReduxProvider: React.SFC = (props) => {
    return (
        <Provider store={store}>
        {
            props.children
        }
        </Provider>
    )
}

export default ReduxProvider