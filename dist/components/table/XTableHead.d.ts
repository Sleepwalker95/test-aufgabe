import React from 'react';
export interface RowItem {
    id: number;
    numeric: boolean;
    label: string;
    canSortBy?: boolean;
}
declare const XTableHead: React.SFC<{
    orderDescending?: boolean;
    orderBy?: number;
    headRows: RowItem[];
    onSortByClick?: (row: RowItem, orderDescending: boolean) => void;
}>;
export default XTableHead;
