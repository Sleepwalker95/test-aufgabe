import React from 'react';
import XTable from './table/XTable';
import CopyIdButton from './CopyIdButton';
import moment from 'moment';
import { Avatar } from '@material-ui/core';
import ProfilePlaceholderIcon from '@material-ui/icons/Portrait';
import sampleDataJson from '../assets/sample_data_stripped.json';
const TableOfUsersWithSampleData = (props) => {
    const sampleData = sampleDataJson['users'] || [];
    const rowData = [
        { id: 0, key: 'id', label: 'Id', numeric: false, transformData: (data) => React.createElement(CopyIdButton, { copyId: data }) },
        { id: 7, key: 'picture', label: 'Profile picture', numeric: false, transformData: (data) => React.createElement(Avatar, { src: data['thumbnail'] },
                React.createElement(ProfilePlaceholderIcon, null)) },
        { id: 1, key: 'firstname', label: 'First name', numeric: false, canSortBy: true },
        { id: 2, key: 'lastname', label: 'Last name', numeric: false, canSortBy: true },
        { id: 3, key: 'username', label: 'Username', numeric: false, canSortBy: true },
        { id: 4, key: 'email', label: 'Email', numeric: false, canSortBy: true },
        { id: 5, key: 'birthdate', label: 'Birthdate', numeric: false, canSortBy: true, transformData: (data) => {
                try {
                    const date = new Date(data);
                    const localizedDateString = date.toLocaleDateString();
                    return localizedDateString;
                }
                catch (err) {
                }
                return data;
            } },
        { id: 6, key: 'birthdate', label: 'Age', numeric: true, canSortBy: true, transformData: (data) => {
                try {
                    var momentDate = moment(new Date(data));
                    var momentNow = moment();
                    const years = momentNow.diff(momentDate, 'years');
                    return years;
                }
                catch (err) {
                }
                return data;
            } },
    ];
    return (React.createElement(XTable, { data: sampleData, rows: rowData, tableId: props.tableId || "sampleData", withSearch: props.withSearch }));
};
export default TableOfUsersWithSampleData;
//# sourceMappingURL=TableOfUsersWithSampleData.js.map