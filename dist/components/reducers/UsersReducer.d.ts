export interface UsersState {
    [tableId: string]: {
        sortRowId?: number;
        descending?: boolean;
        filter?: string;
    };
}
declare const ACTION_SORT_BY = "actions-users-sort-by";
declare const ACTION_FILTER = "actions-users-filter";
interface ActionSortBy {
    type: typeof ACTION_SORT_BY;
    tableId: string;
    sortRowId?: number;
    descending?: boolean;
}
interface ActionFilter {
    type: typeof ACTION_FILTER;
    tableId: string;
    filter?: string;
}
export declare type Actions = ActionSortBy | ActionFilter;
export declare function sortUsersBy(tableId: string, sortRowId?: number, descending?: boolean): ActionSortBy;
export declare function filterUsers(tableId: string, filter?: string): ActionFilter;
export default function UsersReducer(state: UsersState | undefined, action: Actions): UsersState;
export {};
