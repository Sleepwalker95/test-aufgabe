import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
ReactDOM.render((React.createElement(BrowserRouter, null,
    React.createElement(Switch, null,
        React.createElement(Route, { path: "/:path", component: App }),
        React.createElement(Route, { render: () => React.createElement(Redirect, { to: "/home" }) })))), document.getElementById('root'));
serviceWorker.unregister();
//# sourceMappingURL=index.js.map