export interface UsersState {
    [tableId: string]: {
        sortRowId?: number,
        descending?: boolean,
        filter?: string
    }
}
const initialState: UsersState = { 
    //empty
}

const ACTION_SORT_BY = "actions-users-sort-by"
const ACTION_FILTER = "actions-users-filter"
interface ActionSortBy {
    type: typeof ACTION_SORT_BY,
    tableId: string, 
    sortRowId?: number,
    descending?: boolean
}
interface ActionFilter {
    type: typeof ACTION_FILTER,
    tableId: string, 
    filter?: string
}
export type Actions = ActionSortBy | ActionFilter

export function sortUsersBy(tableId: string, sortRowId?: number, descending?: boolean): ActionSortBy {
    return {
        type: ACTION_SORT_BY,
        tableId,
        sortRowId, descending
    }
}
export function filterUsers(tableId: string, filter?: string): ActionFilter {
    return {
        type: ACTION_FILTER,
        tableId,
        filter
    }
}


export default function UsersReducer(state = initialState, action: Actions): UsersState {
    switch(action.type) {
        case ACTION_SORT_BY: {
            const sortAction = action as ActionSortBy
            var sortPayload = { ...state }
            sortPayload[action.tableId] = {
                ...sortPayload[action.tableId],
                sortRowId: sortAction.sortRowId, descending: sortAction.descending
            }
            return sortPayload
        }
        case ACTION_FILTER: {
            const filterAction = action as ActionFilter
            var filterPayload = { ...state }
            filterPayload[action.tableId] = {
                ...filterPayload[action.tableId],
                filter: filterAction.filter
            }
            return filterPayload
        }
    } 
    return state
}