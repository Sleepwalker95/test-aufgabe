import { combineReducers } from "redux";
import UsersReducer from "./UsersReducer";

const rootReducer = combineReducers({
    usersConfig: UsersReducer
})
export default rootReducer

export type ReduxAppState = ReturnType<typeof rootReducer>