import React from 'react';
import './App.css';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { CssBaseline, Container } from '@material-ui/core';
import TableOfUsersWithSampleData from './components/TableOfUsersWithSampleData';

const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1,
		minHeight: '100vh'
	},
	paper: {
		padding: theme.spacing(3, 2),
	},
	container: {
		padding: theme.spacing(3, 0),
	}
}));

const App: React.FC = () => {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<CssBaseline />
			<AppBar position="static" color="primary">
				<Toolbar>
					<Typography variant="h6" color="inherit">
						Users
					</Typography>
				</Toolbar>
			</AppBar>
      		<Container fixed className={classes.container}>

				<TableOfUsersWithSampleData withSearch />

			</Container>
		</div>
	)
}

export default App;