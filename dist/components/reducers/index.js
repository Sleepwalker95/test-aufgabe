import { combineReducers } from "redux";
import UsersReducer from "./UsersReducer";
const rootReducer = combineReducers({
    usersConfig: UsersReducer
});
export default rootReducer;
//# sourceMappingURL=index.js.map