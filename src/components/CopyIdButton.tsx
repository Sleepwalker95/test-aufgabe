import React, { useState } from 'react';
import { Button, Snackbar, IconButton, makeStyles } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

const XSnackbar: React.FC<{text: string, children: (show: ()=>void)=>React.ReactNode}> = (props) => {
    const { text, children } = props

    const [visible, setVisible] = useState(false)

    const show = ()=>setVisible(true)
    const hide = ()=>setVisible(false)

    const classes = makeStyles(theme => ({
        close: {
          padding: theme.spacing(0.5),
        },
    }))();

    return (
        <>
        { children(show) }
        <Snackbar
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'left',
            }}
            open={visible}
            autoHideDuration={3000}
            onClose={hide}
            ContentProps={{
                'aria-describedby': 'message-id',
            }}
            message={<span id="message-id">{text}</span>}
            action={[
                <IconButton
                    key="close"
                    aria-label="Close"
                    color="inherit"
                    className={classes.close}
                    onClick={hide}>
                    <CloseIcon />
                </IconButton>,
            ]}
        />
        </>
    )
}

const CopyIdButton: React.SFC<{copyId: string, buttonTitle?: string}> = (props) => {
    return (
        <XSnackbar text="Copied to clipboard!">
        {(showSnackbar)=>(
            <Button variant="outlined" size="small" color="primary" onClick={()=>{
                if(Boolean(props.copyId)) {
                    navigator.clipboard.writeText(props.copyId)
                    showSnackbar && showSnackbar()
                }
            }}>{props.buttonTitle || "Copy"}</Button>
        )}
        </XSnackbar>
    )
}

export default CopyIdButton