import React from 'react';
import ReduxProvider from '../redux/ReduxProvider';
import { makeStyles, TableBody, TableRow, TableCell, Table, Paper } from '@material-ui/core';
import XTableHead from './XTableHead';
import { shallowEqual, useSelector, useDispatch } from 'react-redux';
import { sortUsersBy, filterUsers } from '../reducers/UsersReducer';
import XSearchField from './XSearchField';
const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    paper: {
        padding: theme.spacing(1, 1),
    },
    table: {
        overflowX: 'scroll',
    },
}));
const XTable = (props) => {
    const { data, rows, tableId, withSearch } = props;
    const classes = useStyles();
    const dispatch = useDispatch();
    const sortRowId = useSelector((state) => state.usersConfig[tableId] && state.usersConfig[tableId].sortRowId, shallowEqual);
    const orderDescending = useSelector((state) => state.usersConfig[tableId] && state.usersConfig[tableId].descending, shallowEqual);
    const searchString = useSelector((state) => state.usersConfig[tableId] && state.usersConfig[tableId].filter, shallowEqual);
    const sortRow = rows.filter(row => row.id === sortRowId);
    const sortKey = sortRow && sortRow.length > 0 && sortRow[0].key;
    const searchFilter = (value, index) => {
        if (searchString) {
            const filterBy = searchString.trim().toLowerCase();
            if (filterBy && filterBy.length > 0) {
                var containsSearchString = false;
                Object.keys(value).forEach(key => {
                    if (value[key] && typeof value[key] === 'string') {
                        if (filterBy.length > 3 ? value[key].toLowerCase().includes(filterBy) : value[key].toLowerCase().startsWith(filterBy)) {
                            containsSearchString = true;
                        }
                    }
                    if (containsSearchString) {
                        return;
                    }
                });
                return containsSearchString;
            }
        }
        return true;
    };
    const sortComparator = (a, b) => {
        try {
            if (sortKey) {
                if (orderDescending) {
                    return b[sortKey].localeCompare(a[sortKey]);
                }
                return a[sortKey].localeCompare(b[sortKey]);
            }
        }
        catch (err) {
        }
        return 0;
    };
    return (React.createElement("div", { className: classes.root },
        Boolean(withSearch) &&
            React.createElement(Paper, { className: classes.paper },
                React.createElement(XSearchField, { value: searchString, onChange: (value) => {
                        dispatch(filterUsers(tableId, value));
                    } })),
        React.createElement("div", { className: classes.table },
            React.createElement(Table, null,
                React.createElement(XTableHead, { headRows: rows, orderBy: sortRowId, orderDescending: orderDescending, onSortByClick: (row, orderDescending) => {
                        dispatch(sortUsersBy(tableId, row.id, orderDescending));
                    } }),
                React.createElement(TableBody, null, data.filter(searchFilter).sort(sortComparator).map((item, index) => {
                    return (React.createElement(TableRow, { key: index }, rows.map(row => (React.createElement(TableCell, { key: row.id, align: row.numeric ? 'right' : 'left' }, (row.transformData && row.transformData(item[row.key])) || item[row.key])))));
                }))))));
};
export default (props) => {
    return (React.createElement(ReduxProvider, null,
        React.createElement(XTable, Object.assign({}, props))));
};
//# sourceMappingURL=XTable.js.map