import React from 'react'
import ReduxProvider from '../redux/ReduxProvider';
import { makeStyles, TableBody, TableRow, TableCell, Table, Paper } from '@material-ui/core';
import XTableHead, { RowItem as XRowItem } from './XTableHead';
import { shallowEqual, useSelector, useDispatch } from 'react-redux'
import { ReduxAppState } from '../reducers';
import { sortUsersBy, filterUsers } from '../reducers/UsersReducer';
import XSearchField from './XSearchField';

const useStyles = makeStyles(theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing(3),
    },
    paper: {
        padding: theme.spacing(1, 1),
    },
    table: {
        overflowX: 'scroll',
    },
}))

export interface RowItem extends XRowItem {
    key: string,
    transformData?: (data: any)=>React.ReactNode
}

interface Props {
    data: any[], 
    rows: RowItem[], 
    tableId: string,
    withSearch?: boolean
}
const XTable: React.FC<Props> = (props) => {
    const { data, rows, tableId, withSearch } = props
    const classes = useStyles();
    const dispatch = useDispatch()

    const sortRowId = useSelector((state: ReduxAppState)=>state.usersConfig[tableId] && state.usersConfig[tableId].sortRowId, shallowEqual)
    const orderDescending = useSelector((state: ReduxAppState)=>state.usersConfig[tableId] && state.usersConfig[tableId].descending, shallowEqual)
    const searchString = useSelector((state: ReduxAppState)=>state.usersConfig[tableId] && state.usersConfig[tableId].filter, shallowEqual)

    const sortRow = rows.filter(row=>row.id===sortRowId)
    const sortKey = sortRow && sortRow.length>0 && sortRow[0].key

    const searchFilter = (value, index)=>{
        if(searchString) {
            const filterBy = searchString.trim().toLowerCase()
            if(filterBy && filterBy.length>0) {
                var containsSearchString = false
                Object.keys(value).forEach(key => {
                    if(value[key] && typeof value[key] === 'string') {      //TODO add searchable option to field
                        //do a partial search when search string has at least 4 characters
                        if(filterBy.length>3 ? value[key].toLowerCase().includes(filterBy) : value[key].toLowerCase().startsWith(filterBy)) {
                            containsSearchString = true
                        }
                    }
                    if(containsSearchString) {
                        return
                    }
                })
                return containsSearchString
            }
        }
        return true
    }

    const sortComparator = (a,b)=>{
        try {
            if(sortKey) {
                if(orderDescending) {
                    return b[sortKey].localeCompare(a[sortKey])
                }
                return a[sortKey].localeCompare(b[sortKey])
            } 
        }
        catch(err) {
            //
        }
        return 0
    }

    return (
        <div className={classes.root}>
            {
                Boolean(withSearch) &&
                <Paper className={classes.paper}>
                    <XSearchField value={searchString} onChange={(value)=>{
                        dispatch(filterUsers(tableId, value))     //dispatch redux action
                    }} />
                </Paper>
            }
            <div className={classes.table}>
                <Table>    
                    <XTableHead headRows={rows} orderBy={sortRowId} orderDescending={orderDescending} onSortByClick={(row, orderDescending)=>{
                        dispatch(sortUsersBy(tableId, row.id, orderDescending))     //dispatch redux action
                    }} />
                    <TableBody>
                    {data.filter(searchFilter).sort(sortComparator).map((item, index) => {
                        return (
                            <TableRow key={index}>
                            {rows.map(row => (
                                <TableCell key={row.id} align={row.numeric ? 'right' : 'left'}>
                                {
                                    (row.transformData && row.transformData(item[row.key])) || item[row.key]
                                }
                                </TableCell>
                            ))}
                            </TableRow>
                        )
                    })}
                    </TableBody>
                </Table>
            </div>
        </div>
    )
}

export default (props: Props)=>{
    return (
        <ReduxProvider>
            <XTable {...props} />
        </ReduxProvider>
    )
}