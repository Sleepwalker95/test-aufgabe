import React from 'react';
declare const XSearchField: React.SFC<{
    value: string;
    onChange: (value: string) => void;
}>;
export default XSearchField;
