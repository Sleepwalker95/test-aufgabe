import React, { useState } from 'react';
import { Button, Snackbar, IconButton, makeStyles } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
const XSnackbar = (props) => {
    const { text, children } = props;
    const [visible, setVisible] = useState(false);
    const show = () => setVisible(true);
    const hide = () => setVisible(false);
    const classes = makeStyles(theme => ({
        close: {
            padding: theme.spacing(0.5),
        },
    }))();
    return (React.createElement(React.Fragment, null,
        children(show),
        React.createElement(Snackbar, { anchorOrigin: {
                vertical: 'top',
                horizontal: 'left',
            }, open: visible, autoHideDuration: 3000, onClose: hide, ContentProps: {
                'aria-describedby': 'message-id',
            }, message: React.createElement("span", { id: "message-id" }, text), action: [
                React.createElement(IconButton, { key: "close", "aria-label": "Close", color: "inherit", className: classes.close, onClick: hide },
                    React.createElement(CloseIcon, null)),
            ] })));
};
const CopyIdButton = (props) => {
    return (React.createElement(XSnackbar, { text: "Copied to clipboard!" }, (showSnackbar) => (React.createElement(Button, { variant: "outlined", size: "small", color: "primary", onClick: () => {
            if (Boolean(props.copyId)) {
                navigator.clipboard.writeText(props.copyId);
                showSnackbar && showSnackbar();
            }
        } }, props.buttonTitle || "Copy"))));
};
export default CopyIdButton;
//# sourceMappingURL=CopyIdButton.js.map